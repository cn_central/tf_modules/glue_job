variable "name" {
    description = "Unique name of the job"
}

variable "region" {
  default = "us-east-1"
}

variable "enabled" {
    description =   "enables the create of Glue job"
    default     =   true
}

variable "description" {
    description =   "Description of the job"
    default     =   "Created by terraform"
}

variable "timeout" {
    description = "Job timeout in minutes"
    default = 2880
}

variable "security_configuration" {
    description = " Security configuration to be associated with the job"
}

variable "job_language" {
    description = "Glue job language"
    default     =  "python"
}

variable "glue_version" {
    description = "Glue job version"
}

variable "python_version" {
    description     =   "Phython version"
    default         =   ""
}


variable "connections" {
    description =   "List of connections used for this job"
    type        =   list(string)
    default     =   []
}

variable "max_retries" {
    description =   "Maximum number of times to retry job if it fails"
}
variable "max_concurrent_runs" {
    description =   "Maximum concurrent runs allowed"
    default     =   1
}
variable "worker_max_capacity" {
    description =   "Max capacity of the worker"
    default     =   1
}

variable "worker_type" {
    description = "Type of worker job"
    default     = "Standard"
}

variable "worker_count" {
    description = "Number of workers"
    default     = 3
}

variable "command_name" {
    description =   "Name of the job command.  For Spark, this must be glueetl.  For Python Shell, this must be pythonshell"
}

variable "command_script" {
type = map(string)
description = "location of the job script"

}

variable "extra_jars" {
    type        = map(string)
    description = "location of jar files"

    default = {
        location    = ""
        bucket_arn  = ""
     //   kms_key_arn = ""
    }
}

variable "temp_dir" {
    type        = map(string)
    description = "location of temp files"

    default = {
        location    = ""
        bucket_arn  = ""
     //   kms_key_arn = ""
    }
}

variable "scala_class_name" {
    description ="Scala class name"
    default     = ""
}

variable"additional_default_arguments" {
    description = "Additional job parameters"
    default     = {}
}

locals {

    command_name = {
        command_name = var.command_name
    }
    worker_max_capacity =   var.command_name =="pythonshell" || var.command_name == "glueetl" ? var.worker_max_capacity : 10

    temp_dir_location = lookup(var.temp_dir,"location","" )


    _extra_jars_bucket_arn = lookup(
    var.extra_jars,
    "bucket_arn",
    var.command_script["bucket_arn"],
    )
/*
    _extra_jars_kms_key_arn = lookup(
        var.extra_jars,
        "kms_key_arn",
        var.command_script["kms_key_arn"],
    )
*/
    extra_jars_location     =   lookup(var.extra_jars,"location", "" )
    extra_jars_bucket_arn   =   local._extra_jars_bucket_arn != "" ? local._extra_jars_bucket_arn : var.command_script["bucket_arn"]
  //  extra_jars_kms_key_arn  =   local._extra_jars_kms_key_arn != "" ? local._extra_jars_kms_key_arn : var.command_script["kms_key_arn"]

    default_arguments = {
        "--job-language"            = var.job_language
        "--job-bookmark-option"     = var.job_language
       // "--extra-jars"              = local.extra_jars_location
        "--TempDir"                 = local.temp_dir_location
        "encryption-type"           = "sse-s3"
    }

    scala_default_arguments = {
        "--job-language"            = var.job_language
        "--class"                   = var.scala_class_name
        "--job-bookmark-option"     = var.job_language
     //   "--extra-jars"              = local.extra_jars_location
        "--TempDir"                 = local.temp_dir_location
        "encryption-type"           = "sse-s3"
    }

    special_arguments = {
        "--max-capacity"            = var.worker_max_capacity
        "--worker-type"             = var.worker_type
        "--number-of-workers"       = var.worker_count
    }


}
