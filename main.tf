resource "aws_glue_job" "this" {
    count                   = var.enabled == true ? 1 : 0
    name                    = var.name
    description             = var.description
    role_arn                = element(concat(aws_iam_role.glue_job.*.arn, [""]),0)
    max_retries             = var.max_retries
    timeout                 = var.timeout
    security_configuration  = var.security_configuration
    connections             = var.connections
    max_capacity            = local.worker_max_capacity
    glue_version            = var.glue_version
    default_arguments       = merge(var.additional_default_arguments, var.command_name == "python" ? local.default_arguments : local.scala_default_arguments)

    execution_property {
        max_concurrent_runs =  var.max_concurrent_runs
    }

    dynamic "command" {
        for_each = var.command_name == "pythonshell" ?  local.command_name : {}
        content {
            name                =   var.command_name
            script_location     =   var.command_script["location"]
            python_version      =   var.python_version
        }
    }

    dynamic "command" {
        for_each = var.command_name == "glueetl"  ? local.command_name : {}
        content {
            name            =   var.command_name
            script_location =   var.command_script["location"]
        }
    }

}
