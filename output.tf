output "id" {
  description = "id of the glue job"
  value       = element(concat(aws_glue_job.this.*.id, [""]),0 )
}

output "name" {
  description = "Name of glue job"
  value       = element(concat(aws_glue_job.this.*.name, [""]),0 )
}