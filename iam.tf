resource "aws_iam_role" "glue_job"{
    count   =   var.enabled == true ? 1 : 0
    name    =   "glue-job-${var.name}"

    assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
        "Action": "sts:AssumeRole",
        "Principal": {
            "Service": "glue.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
        }
    ]
})
}
